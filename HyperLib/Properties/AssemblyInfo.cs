﻿using System.Reflection;
using System.Runtime.CompilerServices;

// Information about this assembly is defined by the following attributes. 
// Change them to the values specific to your project.

[assembly: AssemblyTitle("HyperLib")]
[assembly: AssemblyDescription("HyperLib is a C# .Net Framework Library")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("ATVG-Studios")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("Copyright 2015-2017 ATVG-Studios")]
[assembly: AssemblyTrademark("Open Source Project License")]
[assembly: AssemblyCulture("")]

// The assembly version has the format "{Major}.{Minor}.{Build}.{Revision}".
// The form "{Major}.{Minor}.*" will automatically update the build and revision,
// and "{Major}.{Minor}.{Build}.*" will update just the revision.

[assembly: AssemblyVersion("3.0.0.1")]

// The following attributes are used to specify the signing key for the assembly, 
// if desired. See the Mono documentation for more information about signing.

//[assembly: AssemblyDelaySign(false)]
//[assembly: AssemblyKeyFile("")]
