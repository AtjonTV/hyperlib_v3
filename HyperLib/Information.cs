﻿using System;
namespace HyperLib
{
    public class Information
    {
		public static String LibraryName        { get { return "HyperLib";              } }
		public static String LibraryCreator     { get { return "AtjonTV";               } }
		public static String LibraryFramework   { get { return ".net Framework 4.5.2";  } }
		public static String LibraryVersion     { get { return "3.0.0";                 } }
		public static int    LibraryVersionID   { get { return 1;                       } }
    }
}
