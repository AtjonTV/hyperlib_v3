﻿using System;
namespace HyperLib
{
    public class RandomGenerator
    {
		private RandomGeneratorLength Type;

		public RandomGenerator(RandomGeneratorLength type)
		{
			Type = type;
		}

        public string Generate(RandomGeneratorCharset Charset)
		{
			if (Charset == RandomGeneratorCharset.All)
			{
                if (Type == RandomGeneratorLength.Len4)
					return genAll(4);
                if (Type == RandomGeneratorLength.Len8)
					return genAll(8);
                if (Type == RandomGeneratorLength.Len16)
					return genAll(16);
                if (Type == RandomGeneratorLength.Len32)
					return genAll(32);
                if (Type == RandomGeneratorLength.Len64)
					return genAll(64);
				if (Type == RandomGeneratorLength.Len128)
					return genAll(128);
			}

            if (Charset == RandomGeneratorCharset.NumsLettLwrUpr)
			{
				if (Type == RandomGeneratorLength.Len4)
					return genNLU(4);
				if (Type == RandomGeneratorLength.Len8)
					return genNLU(8);
				if (Type == RandomGeneratorLength.Len16)
					return genNLU(16);
				if (Type == RandomGeneratorLength.Len32)
					return genNLU(32);
				if (Type == RandomGeneratorLength.Len64)
					return genNLU(64);
				if (Type == RandomGeneratorLength.Len128)
					return genNLU(128);
			}

			return "An Error Accoured while processing 'GeneratorType' which was given with the Constructor";
		}


		private string genAll(int length)
		{
			System.Random rnd = new System.Random();
			string key = "";
			for (int i = 0; i < (length - 1); i++)
			{
				int chr = 0;
				int rnda = rnd.Next(0, 4);
				switch (rnda)
				{
					case 0:
						chr = rnd.Next(48, 57);
						key += (char)chr;
						break;
					case 1:
						chr = rnd.Next(65, 90);
						key += (char)chr;
						break;
					case 2:
						chr = rnd.Next(97, 122);
						key += (char)chr;
						break;
					case 3:
						chr = rnd.Next(35, 38);
						key += (char)chr;
						break;
				}
			}
			return key;
		}

		private string genNLU(int length)
		{
			System.Random rnd = new System.Random();
			string key = "";
			for (int i = 0; i < (length - 1); i++)
			{
				int chr = 0;
				int rnda = rnd.Next(0, 3);
				switch (rnda)
				{
					case 0:
						chr = rnd.Next(48, 57);
						key += (char)chr;
						break;
					case 1:
						chr = rnd.Next(65, 90);
						key += (char)chr;
						break;
					case 2:
						chr = rnd.Next(97, 122);
						key += (char)chr;
						break;
				}
			}
			return key;
		}
	}

    public enum RandomGeneratorLength
    {
        Len4,
        Len8,
        Len16,
        Len32,
        Len64,
        Len128
    }

    public enum RandomGeneratorCharset
    {
        All,
        NumsLettLwrUpr
    }
}